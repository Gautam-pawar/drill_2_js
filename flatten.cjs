
let finalArray = [];
function flatten(elements) {


    if(!Array.isArray(elements)){

        return [] ;

    }
    for (let index = 0; index < elements.length; index++) {

        if (Array.isArray(elements[index])) {

            flatten(elements[index]);

        } else {

            finalArray.push(elements[index]);
        }

    }

    return finalArray;

}

module.exports = flatten;