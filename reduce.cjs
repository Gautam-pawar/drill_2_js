function reduce(elements, cb, startingValue, memo = null) {

    if (typeof cb !== "function" || !Array.isArray(elements)) {

        return undefined;
    }

    let accumulator = startingValue;

    let start = 0;

    if (startingValue === undefined) {

        accumulator = elements[0];

        start = 1;

    }

    for (let index = start; index < elements.length; index++) {

        accumulator = cb(accumulator, elements[index]);

    }

    return accumulator;


}

module.exports = reduce;