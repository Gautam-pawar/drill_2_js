function find(elements, cb) {


    if(!Array.isArray(elements) || elements.length === 0  || typeof cb !== "function" ){

        return undefined ;
    }

    let Answer ;

    for (let index = 0; index < elements.length; index++) {

        if (cb(elements[index])) {

           return  Answer = elements[index];

        }
    }
        return undefined;

}

module.exports = find;