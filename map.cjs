function map(elements, cb) {

    if(!Array.isArray(elements) || typeof cb !== "function"){

        return [] ;
    }

    let resultantArray = [];

    for (let index = 0; index < elements.length; index++) {

        let updatedValue = cb(elements[index], index,elements);

        resultantArray.push(updatedValue);
    }

    return resultantArray;
}

module.exports = map;
