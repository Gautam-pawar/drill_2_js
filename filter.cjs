function filter(elements, cb) {

    if (!Array.isArray(elements) || typeof cb !== "function") {

        return [];
    }

    let resultantArray = [];

    for (let index = 0; index < elements.length; index++) {


        if (cb(elements[index])) {

            resultantArray.push(elements[index]);

        }

    }

    return resultantArray;

}

module.exports = filter;