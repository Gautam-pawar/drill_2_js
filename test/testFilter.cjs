const testArray = require(`../arrays.cjs`);

const filterFunction = require(`../filter.cjs`);

function cb(num) {

    if (num%2 !== 0) {
        return true;
    }

    return false;

}

const resut = filterFunction(testArray.testArray, cb);

console.log(resut);