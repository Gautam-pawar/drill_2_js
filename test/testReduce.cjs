const testArray = require(`../arrays.cjs`);

const reduceFunction = require(`../reduce.cjs`);

const startingValue = 10;

function cb(startingValue, current,memo = null) {

    return startingValue * current;

}

const result = reduceFunction(testArray.testArray, cb, startingValue);

console.log(result);