const testArray = require(`../arrays.cjs`);

const findFunction = require(`../find.cjs`);

function cb(num) {

    if (num%2 === 0) {
        return true;
    }

    return false;

}

const result = findFunction(testArray.testArray, cb);

console.log(result);